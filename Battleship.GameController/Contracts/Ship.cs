﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Battleship.GameController.Contracts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The ship.
    /// </summary>
    public class Ship
    {
        private bool isPlaced;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        public Ship()
        {
            Positions = new List<Position>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        public List<Position> Positions { get; set; }

        /// <summary>
        /// The color of the ship
        /// </summary>
        public ConsoleColor Color { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public int Size { get; set; }

        public bool Sunk => Positions.All(p => p.Hit);

        #endregion

        #region Public Methods and Operators

        public static Position CreatePosition(string input) {
            if (input.Length < 2)  throw new ArgumentException("Invalid position input");

            var letterChar = input[0];
            if (char.IsDigit(letterChar)) throw new ArgumentException("Invalid letter input");
            if (!Enum.TryParse(letterChar.ToString().ToUpper(), out Letters letter)) throw new ArgumentException("Invalid letter input");

            var numberChars = input[1..];
            if (!int.TryParse(numberChars, out int number)) throw new ArgumentException("Invalid number input");

            return new Position { Column = letter, Row = number };
        }

        /// <summary>
        /// The add position.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        public void AddPosition(string input)
        {
            if (Positions == null)
            {
                Positions = new List<Position>();
            }
            Positions.Add(CreatePosition(input));
        }

        public bool IsPlaced
        {
            get { return isPlaced; }
            set
            {
                if (value.Equals(isPlaced)) return;
                isPlaced = value;
            }
        }

        public void RenderShip(bool withCoordinates) {
            var shipPositionDescription = withCoordinates ? $" ({string.Join(':', Positions.Select(pos => pos.ToString()).ToList())})" : "";
            var isSunkDescription = Sunk ? " (sunk!)" : "";
            
            Console.ForegroundColor = Color;
            Console.WriteLine($"{Name}{shipPositionDescription}{isSunkDescription}");
            Console.ResetColor();
        }

        #endregion
    }
}
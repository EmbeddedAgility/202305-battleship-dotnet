﻿namespace Battleship.GameController.Contracts
{
    /// <summary>
    /// Letters for the column
    /// </summary>
    public enum Letters
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z
    }
}


namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Text;
    using Battleship.Ascii.TelemetryClient;
    using Battleship.GameController;
    using Battleship.GameController.Contracts;
    using System.Media;

    public class Program
    {
        private static List<Ship> myFleet;

        private static List<Ship> enemyFleet;

        private static ITelemetryClient telemetryClient;

        private static List<Position> computerPlayedPositions = new();


        static void Main()
        {
            telemetryClient = new ApplicationInsightsTelemetryClient();
            telemetryClient.TrackEvent("ApplicationStarted", new Dictionary<string, string> { { "Technology", ".NET" } });

            try
            {
                Console.Title = "Battleship";
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Clear();

                RenderBattleship();

                InitializeGame();

                bool playerWon = StartGame();

                if (playerWon)
                    PlayAnimation();
                else
                    RenderYouLost();

                Console.WriteLine("Game Over, press any key to exit");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("A serious problem occured. The application cannot continue and will be closed.");
                telemetryClient.TrackException(e);
                Console.WriteLine("");
                Console.WriteLine("Error details:");
                throw new Exception("Fatal error", e);
            }
        }

        private static void PlayAnimation()
        {
            SoundPlayer sp = new SoundPlayer("fireworks.wav");
            sp.Play();

            var stages = new string[]{
" \n"+
" \n"+
" \n"+
"                       .|\n"+
"                       | |\n"+
"                       |'|  '         ._____\n"+
"               ___    |  | .          |.   |' .---\"|\n"+
"       _    .-'   '-. |  | .   .--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
" \n"+
" \n"+
"                       .|   _\\/_\n"+
"                       | |   /\\ \n"+
"                       |'|  '         ._____\n"+
"               ___    |  | .          |.   |' .---\"|\n"+
"       _    .-'   '-. |  | .   .--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
" \n"+
"                            *  *\n"+
"                       .|  *_\\/_*\n"+
"                       | | * /\\ *\n"+
"                       |'|  *  *      ._____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     .--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
" \n"+
"                            *  *\n"+
"                       .|  *    * \n"+
"                       | | *    * _\\/_\n"+
"          _\\/_         |'|  *  *   /\\ ._____\n"+
"           /\\  ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     .--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
" \n"+
"                            *  *\n"+
"               _\\/_    .|  *    * .::.\n"+
"          .''.  /\\     | | *     :_\\/_:\n"+
"         :_\\/_:        |'|  *  * : /\\ :_____\n"+
"         : /\\ :___    |  |   o    '::'|.   |' .---\"|\n"+
"       _  '..-'   '-. |  |     .--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
" \n"+
"               .''.          *  \n"+
"              :_\\/_:   .|         .::.\n"+
"          .''.: /\\ :   | |       :    :\n"+
"         :    :'..'    |'|  \\'/  :    :_____\n"+
"         :    :___    |  | = o =  '::'|.   |' .---\"|\n"+
"       _  '..-'   '-. |  |  /.\\.--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__  |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

" \n"+
"                           _\\)/_\n"+
"               .''.         /(\\  \n"+
"              :    :   .|         _\\/_\n"+
"          .''.:    :   | |   :     /\\  \n"+
"         :    :'..'    |'|'.\\'/.'     ._____\n"+
"         :    :___    |  |-= o =-     |.   |' .---\"|\n"+
"       _  '..-'   '-. |  |.'/.\\:--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                             .\n"+
"                           _\\)/_\n"+
"               .''.         /(\\   .''.\n"+
"              :    :   .|    '   :_\\/_:\n"+
"              :    :   | |   :   : /\\ :\n"+
"               '..'    |'|'. ' .' '..'._____\n"+
"               ___    |  |-=   =-     |.   |' .---\"|\n"+
"       _    .-'   '-. |  |.' . :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"  \n"+
" \n"+
"                _\\/_              .''.\n"+
"                 /\\    .|        :    :\n"+
"                       | |       :    :\n"+
"                       |'|        '..'._____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                              \\'/\n"+
"                *  *         = o =\n"+
"               *_\\/_*         /.\\ .''.\n"+
"               * /\\ *  .|        :    :\n"+
"                *  *   | |       :    :\n"+
"                       |'|        '..'._____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                            '.\\'/.'\n"+
"                *  *        -= o =-\n"+
"               *    *       .'/.\\'.\n"+
"               *    *  .|      :\n"+
"                *  *   | | \n"+
"                       |'|            ._____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                            '.\\'/.'\n"+
"                            -=   =-\n"+
"                   o        .'/.\\'.\n"+
"            o          .|      :\n"+
"                       | |        .:.\n"+
"                       |'|        ':' ._____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                            '. ' .'\n"+
"                  \\'/       -     -\n"+
"           \\'/   = o =      .' . '.\n"+
"          = o =   /.\\  .|      :  .:::.\n"+
"           /.\\         | |       :::::::\n"+
"                       |'|        ':::'_____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                   :\n"+
"            :   '.\\'/.'\n"+
"         '.\\'/.'-= o =-           .:::.\n"+
"         -= o =-.'/.\\'..|        :::::::\n"+
"         .'/.\\'.   :   | |       :::::::\n"+
"            :          |'|        ':::'_____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                   :\n"+
"            :   '.\\'/.'\n"+
"         '.\\'/.'-=   =-       *   .:::.\n"+
"         -=   =-.'/.\\'..|        ::' '::\n"+
"         .'/.\\'.   :   | |       ::. .::\n"+
"            :          |'|        ':::'_____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

"                   :          .\n"+
"            :   '. ' .'     _\\)/_\n"+
"         '. ' .'-     -      /(\\  .'''.\n"+
"         -     -.' . '..|     '  :     :\n"+
"         .' . '.   :   | |       :     :\n"+
"            :          |'|        '...'_____\n"+
"               ___    |  |            |.   |' .---\"|\n"+
"       _    .-'   '-. |  |     :--'|  ||   | _|    |\n"+
"    .-'|  _.|  |    ||   '-__: |   |  |    ||      |\n"+
"    |' | |.    |    ||       | |   |  |    ||      |\n"+
" ___|  '-'     '    \"\"       '-'   '-.'    '`      |____\n"+
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",
};

            int i = 0;
            while (true)
            {
                Console.Clear();
                Console.ForegroundColor = (ConsoleColor)(i % stages.Length);

                if (Console.ForegroundColor == ConsoleColor.Black)
                {
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                }

                Console.WriteLine("                        You Won! ");
                Console.Write(stages[i % stages.Length]);
                Thread.Sleep(200);
                i++;
            }
        }

        private static void RenderYouLost()
        {
            var fg = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("       You Lost! ");
            Console.WriteLine();
            Console.WriteLine(@"       ~~~~~~~~~");
            Console.WriteLine(@"     /           \");
            Console.WriteLine(@"    /             \");
            Console.WriteLine(@"   | )           ( |");
            Console.WriteLine(@"    \  /C\   /C\  /");
            Console.WriteLine(@"    /  ~~~   ~~~  \");
            Console.WriteLine(@"    \___  .^,  ___/");
            Console.WriteLine(@"     `| _______ |'");
            Console.WriteLine(@"  _   | HHHHHHH |   _");
            Console.WriteLine(@" ( )  \         /  ( )");
            Console.WriteLine(@"(_) \  ~~~~^~~~~ ,/ (_)");
            Console.WriteLine(@"   \ ;\         /  / ");
            Console.WriteLine(@"     \  \     /  /");
            Console.WriteLine(@"       \  \v/  /");
            Console.WriteLine(@"        >     <");
            Console.WriteLine(@"       /  /^\  \");
            Console.WriteLine(@"     /  /    \  \");
            Console.WriteLine(@"    /  /       \  \");
            Console.WriteLine(@"( ) /            \ ( )");
            Console.WriteLine(@" (_)              (_)");

            Console.ForegroundColor = fg;
        }

        private static void RenderBattleship()
        {
            var fg = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.DarkRed;

            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.WriteLine(@"|                        Welcome to Battleship                         BB-61/");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();

            Console.ForegroundColor = fg;
        }

        private static bool StartGame()
        {
#if DEBUG
//render ship debug menu
#else
            Console.Clear();
#endif

            RenderCannon();

            do
            {
                RenderSeparator();
                RenderSeparator();

                WriteColorLine("Player, it's your turn", ConsoleColor.Cyan);
                Console.Write("Enter coordinates for your shot (A to H and 1 to 8) :");

                var position = ParsePosition(Console.ReadLine());
                Console.WriteLine();
                var isHit = GameController.CheckIsHit(enemyFleet, position);
                telemetryClient.TrackEvent("Player_ShootPosition", new Dictionary<string, string>() { { "Position", position.ToString() }, { "IsHit", isHit.ToString() } });
                if (isHit)
                {
                    Console.Beep();

                    RenderBoom();
                    WriteColorLine("Yeah ! Nice hit !", ConsoleColor.Green);
                }
                else
                {
                    RenderSplash();
                    WriteColorLine("You missed !", ConsoleColor.Red);
                }

                if (GameController.CheckAllSunk(enemyFleet))
                {
                    WriteColorLine("You Win!", ConsoleColor.Green);
                    return true;
                }


                RenderSeparator();

                Console.WriteLine();

                WriteColorLine("Computer is shooting ...", ConsoleColor.Cyan);

                position = GetRandomPosition();
                isHit = GameController.CheckIsHit(myFleet, position);
                telemetryClient.TrackEvent("Computer_ShootPosition", new Dictionary<string, string>() { { "Position", position.ToString() }, { "IsHit", isHit.ToString() } });
                Console.WriteLine();
                //Console.WriteLine($"Computer shot in {position.Column}{position.Row} and {(isHit ? "has hit your ship !" : "missed")}");
                if (isHit)
                {
                    Console.Beep();

                    RenderBoom();
                    WriteColorLine("Computer hit!", ConsoleColor.Red);
                }
                else
                {
                    RenderSplash();
                    WriteColorLine("Computer missed!", ConsoleColor.Green);
                }

                RenderShipsList("Enemy ships:");

                if (GameController.CheckAllSunk(myFleet))
                {
                    Console.WriteLine();
                    Console.WriteLine("You Lost !");

                    return false;
                }
            }
            while (true);
        }

        private static void RenderCannon()
        {
            var fg = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.DarkRed;

            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            Console.ForegroundColor = fg;
        }

        private static void WriteColor(string data, ConsoleColor color)
        {
            var previousColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(data);
            Console.ForegroundColor = previousColor;
        }

        private static void WriteColorLine(string data, ConsoleColor color)
        {
            var previousColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(data);
            Console.ForegroundColor = previousColor;
        }

        public static void RenderShipsList(string title)
        {
            Console.WriteLine(title);

            foreach (var ship in enemyFleet)
            {
                ship.RenderShip(ship.Sunk);
            }
        }

        public static Position ParsePosition(string input)
        {
            var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
            var number = int.Parse(input.Substring(1, 1));
            return new Position(letter, number);
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            Position p;
            do
            {
                var letter = (Letters)random.Next(lines);
                var number = random.Next(rows);
                p = new Position(letter, number);
            } while (computerPlayedPositions.Contains(p));

            computerPlayedPositions.Add(p);
            return p;
        }

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();


            RenderFleet(enemyFleet, "enemy");
            RenderFleet(myFleet, "player");
        }

        private static bool IsPositionOverlaps(Position position)
        {
            var positionOverlaps = myFleet.Any(ship => ship.Positions.Any(shipPos =>
            {
                return shipPos.Column == position.Column && shipPos.Row == position.Row;
            }));

            return positionOverlaps;
        }

        private static bool IsPositionNotInLine(Ship ship, Position newPosition)
        {
            List<Position> positionsToTest = new List<Position>(ship.Positions);
            positionsToTest.Add(newPosition);

            if (positionsToTest.Count < 2)
            {
                return false;
            }

            return !AreSequential(positionsToTest);
        }

        private static void LogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message + ". Enter another position");
            Console.ResetColor();
        }

        // positions should be distinct
        private static Boolean AreSequential(List<Position> positions)
        {
            var col1 = positions[0].Column;
            var col2 = positions[1].Column;
            Boolean isVertical = (col1 == col2);

            if (isVertical)
            {
                if (!positions.All(p => p.Column == col1))
                    return false;

                int maxRow = positions.Max(p => p.Row);
                int minRow = positions.Min(p => p.Row);

                if (positions.Count != maxRow - minRow + 1)
                    return false;

            }
            else
            {
                if (!positions.All(p => p.Row == positions[0].Row))
                    return false;

                int maxCol = positions.Max(p => (int)p.Column);
                int minCol = positions.Min(p => (int)p.Column);

                if (positions.Count != maxCol - minCol + 1)
                    return false;
            }

            return true;
        }

        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();

            WriteColor("Please position your fleet (Game board size is from A to H and 1 to 8) :", ConsoleColor.Cyan);

            foreach (var ship in myFleet)
            {
                Console.WriteLine();

                WriteColorLine($"Please enter the positions for the {ship.Name} (size: {ship.Size})", ConsoleColor.Cyan);
                Console.WriteLine();

                for (var i = 1; i <= ship.Size; i++)
                {
                    string? position = null;
                    Position newPosition = null;
                    do
                    {
                        Console.Write("Enter position {0} of {1} (i.e A3):", i, ship.Size);
                        position = Console.ReadLine();

                        try
                        {
                            newPosition = Ship.CreatePosition(position);
                        }
                        catch (Exception ex)
                        {
                            LogError($"Input error: {ex.Message}");
                            continue;
                        }

                        if (IsPositionOutOfBounds(newPosition))
                        {
                            LogError("Position is out of bounds");
                            continue;
                        }

                        if (IsPositionOverlaps(newPosition))
                        {
                            LogError("Position overlaps with others");
                            continue;
                        }

                        if (IsPositionNotInLine(ship, newPosition))
                        {
                            LogError("Positions should form a line with no gap");
                            continue;
                        }

                        break;

                    } while (true);

                    ship.AddPosition(position);
                    telemetryClient.TrackEvent("Player_PlaceShipPosition", new Dictionary<string, string>() { { "Position", position }, { "Ship", ship.Name }, { "PositionInShip", i.ToString() } });
                }
            }
        }

        private static bool IsPositionOutOfBounds(Position newPosition)
        {
            return newPosition.Column < Letters.A || newPosition.Column > Letters.H || newPosition.Row < 1 || newPosition.Row > 8;
        }

        private static void Preset1()
        {

            var carrier = enemyFleet.FirstOrDefault(d => d.Name == "Aircraft Carrier");
            if (carrier != null)
            {
                carrier.Positions.Add(new Position { Column = Letters.B, Row = 4 });
                carrier.Positions.Add(new Position { Column = Letters.B, Row = 5 });
                carrier.Positions.Add(new Position { Column = Letters.B, Row = 6 });
                carrier.Positions.Add(new Position { Column = Letters.B, Row = 7 });
                carrier.Positions.Add(new Position { Column = Letters.B, Row = 8 });
            }

            var battleship = enemyFleet.FirstOrDefault(d => d.Name == "Battleship");
            if (battleship != null)
            {
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 5 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 6 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 7 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 8 });
            }

            var submarine = enemyFleet.FirstOrDefault(d => d.Name == "Submarine");
            if (submarine != null)
            {
                submarine.Positions.Add(new Position { Column = Letters.A, Row = 3 });
                submarine.Positions.Add(new Position { Column = Letters.B, Row = 3 });
                submarine.Positions.Add(new Position { Column = Letters.C, Row = 3 });
            }

            var destroyer = enemyFleet.FirstOrDefault(d => d.Name == "Destroyer");
            if (destroyer != null)
            {
                destroyer.Positions.Add(new Position { Column = Letters.F, Row = 8 });
                destroyer.Positions.Add(new Position { Column = Letters.G, Row = 8 });
                destroyer.Positions.Add(new Position { Column = Letters.H, Row = 8 });
            }

            var patrolBoat = enemyFleet.FirstOrDefault(d => d.Name == "Patrol Boat");
            if (patrolBoat != null)
            {
                patrolBoat.Positions.Add(new Position { Column = Letters.C, Row = 5 });
                patrolBoat.Positions.Add(new Position { Column = Letters.C, Row = 6 });
            }
        }

        private static void Preset2()
        {
            var carrier = enemyFleet.FirstOrDefault(d => d.Name == "Aircraft Carrier");
            if (carrier != null)
            {
                carrier.Positions.Add(new Position { Column = Letters.A, Row = 4 });
                carrier.Positions.Add(new Position { Column = Letters.A, Row = 5 });
                carrier.Positions.Add(new Position { Column = Letters.A, Row = 6 });
                carrier.Positions.Add(new Position { Column = Letters.A, Row = 7 });
                carrier.Positions.Add(new Position { Column = Letters.A, Row = 8 });
            }

            var battleship = enemyFleet.FirstOrDefault(d => d.Name == "Battleship");
            if (battleship != null)
            {
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 1 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 2 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 3 });
                battleship.Positions.Add(new Position { Column = Letters.E, Row = 4 });
            }

            var submarine = enemyFleet.FirstOrDefault(d => d.Name == "Submarine");
            if (submarine != null)
            {
                submarine.Positions.Add(new Position { Column = Letters.B, Row = 3 });
                submarine.Positions.Add(new Position { Column = Letters.C, Row = 3 });
                submarine.Positions.Add(new Position { Column = Letters.D, Row = 3 });
            }

            var destroyer = enemyFleet.FirstOrDefault(d => d.Name == "Destroyer");
            if (destroyer != null)
            {
                destroyer.Positions.Add(new Position { Column = Letters.E, Row = 8 });
                destroyer.Positions.Add(new Position { Column = Letters.F, Row = 8 });
                destroyer.Positions.Add(new Position { Column = Letters.G, Row = 8 });
            }

            var patrolBoat = enemyFleet.FirstOrDefault(d => d.Name == "Patrol Boat");
            if (patrolBoat != null)
            {
                patrolBoat.Positions.Add(new Position { Column = Letters.C, Row = 5 });
                patrolBoat.Positions.Add(new Position { Column = Letters.C, Row = 6 });
            }
        }

        private static void RenderFleet(IEnumerable<Ship> fleet, string fleetName) {
            var rowsCount = 8;

            Console.WriteLine();

            Console.WriteLine($"----- Fleet '{fleetName}' positions debug rendering ----- ");
            Console.WriteLine("   ABCDEFGH");

            for (var i = 1; i <= rowsCount; i++)
            {
                RenderTheRow(fleet, i);
            }

            Console.WriteLine($"----- Fleet '{fleetName}' positions debug rendering END ----- ");
            Console.WriteLine();
        }

        private static void RenderTheRow(IEnumerable<Ship> fleet, int row) {
            Letters[] letters = new[] 
            { 
                Letters.A,
                Letters.B,
                Letters.C,
                Letters.D,
                Letters.E,
                Letters.F,
                Letters.G,
                Letters.H,
            };

            Console.Write($"{row.ToString().PadLeft(2, '0')} ");
            foreach (var letter in letters) {

                Position posToTest = new Position(letter, row);

                var isShipPosition = fleet.Any(ship => ship.Positions.Any(shipCellPosition =>
                {
                    return posToTest.Column == shipCellPosition.Column && posToTest.Row == shipCellPosition.Row;
                }));

                if (isShipPosition)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("X");
                }
                else {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("0");
                }

                Console.ResetColor();
            }

            Console.WriteLine();
        }

        private static int ChooseRandomPresetIndex(int strategiesCount) {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            return random.Next(0, strategiesCount);
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();

            // define presets
            List<Action> strategies = new List<Action>() {
                Preset1,
                Preset2
            };

            // choose one randomly
            var strategyToChooseIndex = ChooseRandomPresetIndex(strategies.Count);
            Action strategyToChoose = strategies[strategyToChooseIndex];

            // place ships
            strategyToChoose();
        }

        private static void RenderSplash()
        {
            var fg = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine();
            Console.WriteLine(@"            \       .       .      /");
            Console.WriteLine(@"             \      . .    .      /");
            Console.WriteLine(@"              \     .   .   .    /");
            Console.WriteLine(@"               \  .      .      /");
            Console.WriteLine(@"                \     .   .    /");
            Console.WriteLine(@"                 \    .   .   /");
            Console.WriteLine(@"                  \     .    /");
            Console.WriteLine(@"                   \   .    /");
            Console.WriteLine();

            Console.ForegroundColor = fg;
        }

        private static void RenderBoom()
        {
            var fg = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine();
            Console.WriteLine(@"                \         .  ./");
            Console.WriteLine(@"              \      .:"";'.:..""   /");
            Console.WriteLine(@"                  (M^^.^~~:.'"").");
            Console.WriteLine(@"            -   (/  .    . . \ \)  -");
            Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
            Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
            Console.WriteLine(@"                 -\  \     /  /-");
            Console.WriteLine(@"                   \  \   /  /");
            Console.WriteLine();

            Console.ForegroundColor = fg;
        }

        private static void RenderSeparator()
        {
            var line = string.Join("", Enumerable.Repeat("-", Console.BufferWidth));

            WriteColor(line, ConsoleColor.Gray);
        }
    }
}
